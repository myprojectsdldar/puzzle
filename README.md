# 15-Puzzle Gamme

## Description
15-Puzzle Game:
This repository contains the implementation of the classic 15-Puzzle Game as part of my programming course in the second semester. The 15-Puzzle is a sliding puzzle that consists of a frame(4x4) of numbered square tiles in random order with one tile missing. The objective is to place the tiles in numerical order by making sliding moves that use the empty space. It's written only using Java with focus on learning the javax.swing package

## Visuals
too be implemented..

## Installation
too be implemented..

## Project status
Ongoing.